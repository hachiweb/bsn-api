<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Shippo;


class AuthController extends Controller
{
    //
    public $successStatus = 200;

    public function register(Request $request) {    
		 $validator = Validator::make($request->all(), [ 
		              'name' => 'required',
		              'email' => 'required|email',
		              'password' => 'required',  
		              'c_password' => 'required|same:password', 
		    ]);   
		 if ($validator->fails()) {          
		       return response()->json(['error'=>$validator->errors()], 401);                        }    
		 $input = $request->all();  
		 $input['password'] = bcrypt($input['password']);
		 $user = User::create($input); 
		 $success['token'] =  $user->createToken('AppName')->accessToken;
		 return response()->json(['success'=>$success], $this->successStatus); 
		}

	public function login(){ 
		if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
		   $user = \Auth::user(); 
		   $success['token'] =  $user->createToken('AppName')-> accessToken; 
		    return response()->json(['success' => $success], $this-> successStatus); 
		  } else{ 
		   return response()->json(['error'=>'Unauthorised'], 401); 
		   } 
		}
		  
	public function getUser() {
		 $user = Auth::user();
		 return response()->json(['success' => $user], $this->successStatus); 
		 }	


     public function checkaddress(Request $request)
                {
                    
                  	
                 	require_once(base_path().'/vendor/shippo/shippo-php/lib/Shippo.php');



                 		  \Shippo::setApiKey(env('SHIPPO_PRIVATE'));

                 		//  var_dump($request->get('country'));


                          $fromAddress = \Shippo_Address::create( array(
                            "name" => $request->get('name'),
                            "company" => $request->get('company'),
                              "street1" => $request->get('street1'),
                            "city" => $request->get('postcode'),
                              "city" => $request->get('city'),
                               "country" =>'US',
                            "state" => $request->get('state'),                            
                              "validate" => true,
                            "email" => $request->get('email')
                         ));
                          
   						$result = json_decode($fromAddress);

 					return response()->json(['success' =>$result ], $this->successStatus); 
                      
                             


                }

                  public function getRate(Request $request)
                {


						require_once(base_path().'/vendor/shippo/shippo-php/lib/Shippo.php');



                 		  \Shippo::setApiKey(env('SHIPPO_PRIVATE'));

/*
           
                           $from_address = array(
                                'name' => 'Mr Hippo',
                                'company' => 'Shippo',
                                'street1' => '215 Clayton St.',
                                'city' => 'San Francisco',
                                'state' => 'CA',
                                'zip' => '94117',
                                'country' => 'US',
                                'phone' => '+1 555 341 9393',
                                'email' => 'mr-hippo@goshipppo.com',
                            ); 


                           $to_address = array(
                                'name' => 'Ms Hippo',
                                'company' => 'San Diego Zoo',
                                'street1' => '2920 Zoo Drive',
                                'city' => 'San Diego',
                                'state' => 'CA',
                                'zip' => '92101',
                                'country' => 'US',
                                'phone' => '+1 555 341 9393',
                                'email' => 'ms-hippo@goshipppo.com',
                            );

                           $parcel = array(
                                'length'=> '5',
                                'width'=> '5',
                                'height'=> '5',
                                'distance_unit'=> 'in',
                                'weight'=> '2',
                                'mass_unit'=> 'lb',
                            );


*/

                           // real vaklue

                  $from_address = array(
                                  "name" => $request->get('frname'),
                                "phone" => $request->get('frphone'),
                                "company" => $request->get('frcompany'),
                                  "street1" => $request->get('frstreet1'),
                                "city" => $request->get('frpostcode'),
                                  "city" => $request->get('frcity'),
                                "state" => $request->get('frstate'),
                                 "country" => $request->get('frcountry'),                               
                                "email" => $request->get('fremail')
                             );

                              $to_address = array(
                                  "name" => $request->get('name'),
                                "company" => $request->get('company'),
                                  "street1" => $request->get('street1'),
                                "city" => $request->get('postcode'),
                                  "city" => $request->get('city'),
                                "state" => $request->get('state'),
                                 "country" => $request->get('country'),    
                                  "phone" => $request->get('frphone'),                           
                                "email" => $request->get('email')
                             );


                             $parcel = array(
                                  "length" => $request->get('length'),
                                "width" => $request->get('width'),
                                  "height" => $request->get('height'),
                                "distance_unit" => $request->get('in'),
                                  "weight" => $request->get('weight'),
                                "mass_unit" => $request->get('lb'),
                               
                             );


 



                            $shipment = \Shippo_Shipment::create(
                            array(
                                'address_from'=> $from_address,
                                'address_to'=> $to_address,
                                'parcels'=> array($parcel),
                                'async'=> false,
                            ));
                            // Rates are stored in the `rates` array inside the shipment object
                            $rates = $shipment['rates'];


                            //  $result = json_decode($shipment);

                   // var_dump($result)  ;    


               return response()->json(['success' =>$rates ], $this->successStatus); 

/*
                    return view('rates.getrateprocessp')  
                        ->with('result',$shipment )
                        ->with('rates',$rates );
*/

              }


      function getlabel($index)
          {



						require_once(base_path().'/vendor/shippo/shippo-php/lib/Shippo.php');



                 		  \Shippo::setApiKey(env('SHIPPO_PRIVATE'));

				
				/*
                              require_once('../vendor/shippo/shippo-php/lib/Shippo.php');

                            Shippo::setApiKey("shippo_test_e59aa237e0b0519947985acf993131cd8c93bd7c");

                           $from_address = array(
                                'name' => 'Mr Hippo',
                                'company' => 'Shippo',
                                'street1' => '215 Clayton St.',
                                'city' => 'San Francisco',
                                'state' => 'CA',
                                'zip' => '94117',
                                'country' => 'US',
                                'phone' => '+1 555 341 9393',
                                'email' => 'mr-hippo@goshipppo.com',
                            ); 


                           $to_address = array(
                                'name' => 'Ms Hippo',
                                'company' => 'San Diego Zoo',
                                'street1' => '2920 Zoo Drive',
                                'city' => 'San Diego',
                                'state' => 'CA',
                                'zip' => '92101',
                                'country' => 'US',
                                'phone' => '+1 555 341 9393',
                                'email' => 'ms-hippo@goshipppo.com',
                            );

                           $parcel = array(
                                'length'=> '5',
                                'width'=> '5',
                                'height'=> '5',
                                'distance_unit'=> 'in',
                                'weight'=> '2',
                                'mass_unit'=> 'lb',
                            );




          
             $shipment = \Shippo_Shipment::create(
                            array(
                                'address_from'=> $from_address,
                                'address_to'=> $to_address,
                                'parcels'=> array($parcel),
                                'async'=> false,
                            ));
                            // Rates are stored in the `rates` array inside the shipment object
                            $rates = $shipment['rates'];


            $selected_rate = $rates[$index];

            */
           $selected_rate_object_id = $selected_rate[$index];

            $transaction = \Shippo_Transaction::create(array(
                'rate'=> $selected_rate_object_id,
                'async'=> false,
            ));


  		return response()->json(['success' =>$transaction ], $this->successStatus); 


 			/*
            		return view('rates.getlabel')
                 		->with('transaction',$transaction )  ;


                 */
          }


}
