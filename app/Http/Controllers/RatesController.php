<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Shippo;

use App\shippo_shipment;
use App\ShippoAddress;


class RatesController extends Controller
{
    //
   function index()
    {
    	
//require_once('vendor/autoload.php');
        require_once('../vendor/shippo/shippo-php/lib/Shippo.php');

    Shippo::setApiKey("shippo_test_e59aa237e0b0519947985acf993131cd8c93bd7c");

$fromAddress = array(
    'name' => 'Shawn Ippotle',
    'street1' => '215 Clayton St.',
    'city' => 'San Francisco',
    'state' => 'CA',
    'zip' => '94117',
    'country' => 'US'
);

$toAddress = array(
    'name' => 'Mr Hippo"',
    'street1' => '1 Broadway',
    'city' => 'New York',
    'state' => 'NY',
    'zip' => '10004',
    'country' => 'US',
    'phone' => '+1 555 341 9393'
);

$parcel = array(
    'length'=> '5',
    'width'=> '5',
    'height'=> '5',
    'distance_unit'=> 'in',
    'weight'=> '2',
    'mass_unit'=> 'lb',
);

$shipment = \Shippo_Shipment::create( array(
    'address_from'=> $fromAddress,
    'address_to'=> $toAddress,
    'parcels'=> array($parcel),
    'async'=> false
    )
);



/*
    $address = \Shippo_Address::
        create(
            array(
                 'object_purpose' => 'QUOTE',
                 'name' => 'John Smith',
                 'company' => 'Initech',
                 'street1' => '6512 Greene Rd.',
                 'city' => 'Woodridge',
                 'state' => 'IL',
                 'zip' => '60517',
                 'country' => 'US',
                 'phone' => '773 353 2345',
                 'email' => 'jmercouris@iit.com',
                 'metadata' => 'Customer ID 23424'
            ));
            
        var_dump($address);
 */
   // Get the first rate in the rates results.
// Customize this based on your business logic.
$rate = $shipment["rates"][0];

// Purchase the desired rate.

$transaction = \Shippo_Transaction::create( array( 
    'rate' => $rate["object_id"], 
    'label_file_type' => "PDF", 
    'async' => false ) );
/*
$transaction = \Shippo_Transaction::create( array(
    'shipment' => $shipment,
    'carrier_account' => 'b741b99f95e841639b54272834bc478c',
    'servicelevel_token' => 'usps_priority',
)
);
*/
// Retrieve label url and tracking number or error message
if ($transaction["status"] == "SUCCESS"){
    echo( $transaction["label_url"] );
    echo("\n");
    echo( $transaction["tracking_number"] );
}else {
    echo( $transaction["messages"] );
}

   //var_dump($shipment)  ;

  return view('rates.index')
            ->with('transaction', json_decode($transaction, true));


   
    }

  function mypage()
  {

      return view('rates.mypage');
  }

     public function checkaddress(Request $request)
                {
                    //
                              require_once('../vendor/shippo/shippo-php/lib/Shippo.php');

                            Shippo::setApiKey("shippo_test_e59aa237e0b0519947985acf993131cd8c93bd7c");



                          $fromAddress = \Shippo_Address::create( array(
                            "name" => $request->get('name'),
                            "company" => $request->get('company'),
                              "street1" => $request->get('street1'),
                            "city" => $request->get('postcode'),
                              "city" => $request->get('city'),
                            "state" => $request->get('state'),
                             "country" => $request->get('country'),
                              "validate" => true,
                            "email" => $request->get('email')
                         ));
                          

               
/*
                          $fromAddress = \Shippo_Address::create( array(
                                        "name" => "Shawn Ippotle",
                                        "company" => "Shippo",
                                        "street1" => "215 Clayton St.",
                                        "city" => "San Francisco",
                                        "state" => "CA",
                                        "zip" => "94117",
                                        "country" => "US",
                                        "email" => "shippotle@goshippo.com",
                                        "validate" => true
                                    ));
  */


                     //var_dump($fromAddress)  ;      

                     $result = json_decode($fromAddress);

                   //  var_dump($result->validation_results->is_valid)  ; 

                    //echo $result['is_valid'];
                    //echo $result['is_residential'];
                     //$result->is_complete;

                        //   var_dump($result->is_valid)  ; 

                        return view('rates.checkaddressrep')  
                        ->with('result',$result );
                             


                }

      function getrate()
          {

              return view('rates.getrate');
          }
    public function getrateprocess(Request $request)
                {


                              require_once('../vendor/shippo/shippo-php/lib/Shippo.php');

                            Shippo::setApiKey("shippo_test_e59aa237e0b0519947985acf993131cd8c93bd7c");

                           $from_address = array(
                                'name' => 'Mr Hippo',
                                'company' => 'Shippo',
                                'street1' => '215 Clayton St.',
                                'city' => 'San Francisco',
                                'state' => 'CA',
                                'zip' => '94117',
                                'country' => 'US',
                                'phone' => '+1 555 341 9393',
                                'email' => 'mr-hippo@goshipppo.com',
                            ); 


                           $to_address = array(
                                'name' => 'Ms Hippo',
                                'company' => 'San Diego Zoo',
                                'street1' => '2920 Zoo Drive',
                                'city' => 'San Diego',
                                'state' => 'CA',
                                'zip' => '92101',
                                'country' => 'US',
                                'phone' => '+1 555 341 9393',
                                'email' => 'ms-hippo@goshipppo.com',
                            );

                           $parcel = array(
                                'length'=> '5',
                                'width'=> '5',
                                'height'=> '5',
                                'distance_unit'=> 'in',
                                'weight'=> '2',
                                'mass_unit'=> 'lb',
                            );



                           // real vaklue

/*
                             $from_address = array(
                                  "name" => $request->get('frname'),
                                "phone" => $request->get('frphone'),
                                "company" => $request->get('frcompany'),
                                  "street1" => $request->get('frstreet1'),
                                "city" => $request->get('frpostcode'),
                                  "city" => $request->get('frcity'),
                                "state" => $request->get('frstate'),
                                 "country" => $request->get('frcountry'),                               
                                "email" => $request->get('fremail')
                             );

                              $to_address = array(
                                  "name" => $request->get('name'),
                                "company" => $request->get('company'),
                                  "street1" => $request->get('street1'),
                                "city" => $request->get('postcode'),
                                  "city" => $request->get('city'),
                                "state" => $request->get('state'),
                                 "country" => $request->get('country'),    
                                  "phone" => $request->get('frphone'),                           
                                "email" => $request->get('email')
                             );


                             $parcel = array(
                                  "length" => $request->get('length'),
                                "width" => $request->get('width'),
                                  "height" => $request->get('height'),
                                "distance_unit" => $request->get('in'),
                                  "weight" => $request->get('weight'),
                                "mass_unit" => $request->get('lb'),
                               
                             );


*/
 



                            $shipment = \Shippo_Shipment::create(
                            array(
                                'address_from'=> $from_address,
                                'address_to'=> $to_address,
                                'parcels'=> array($parcel),
                                'async'=> false,
                            ));
                            // Rates are stored in the `rates` array inside the shipment object
                            $rates = $shipment['rates'];


                              $result = json_decode($shipment);

                  // var_dump($result)  ;
                      //var_dump($shipment)  ;        


                    return view('rates.getrateprocessp')  
                        ->with('result',$shipment )
                        ->with('rates',$rates );


              }


      function getLabel($index)
          {



                              require_once('../vendor/shippo/shippo-php/lib/Shippo.php');

                            Shippo::setApiKey("shippo_test_e59aa237e0b0519947985acf993131cd8c93bd7c");

                           $from_address = array(
                                'name' => 'Mr Hippo',
                                'company' => 'Shippo',
                                'street1' => '215 Clayton St.',
                                'city' => 'San Francisco',
                                'state' => 'CA',
                                'zip' => '94117',
                                'country' => 'US',
                                'phone' => '+1 555 341 9393',
                                'email' => 'mr-hippo@goshipppo.com',
                            ); 



                           $to_address = array(
                                'name' => 'Ms Hippo',
                                'company' => 'San Diego Zoo',
                                'street1' => '2920 Zoo Drive',
                                'city' => 'San Diego',
                                'state' => 'CA',
                                'zip' => '92101',
                                'country' => 'US',
                                'phone' => '+1 555 341 9393',
                                'email' => 'ms-hippo@goshipppo.com',
                            );

                           $parcel = array(
                                'length'=> '5',
                                'width'=> '5',
                                'height'=> '5',
                                'distance_unit'=> 'in',
                                'weight'=> '2',
                                'mass_unit'=> 'lb',
                            );




          
             $shipment = \Shippo_Shipment::create(
                            array(
                                'address_from'=> $from_address,
                                'address_to'=> $to_address,
                                'parcels'=> array($parcel),
                                'async'=> false,
                            ));
                            // Rates are stored in the `rates` array inside the shipment object
                            $rates = $shipment['rates'];


            $selected_rate = $rates[$index];
           $selected_rate_object_id = $selected_rate['object_id'];

            $transaction = \Shippo_Transaction::create(array(
                'rate'=> $selected_rate_object_id,
                'async'=> false,
            ));


            return view('rates.getlabel')
                 ->with('transaction',$transaction )
                       ;
          }


    
}
