<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Go Label </title>
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/plugins.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
      <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ asset('plugins/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/dropzone/basic.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/file-upload/file-upload-with-preview.css') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->


    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/table/datatable/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/table/datatable/custom_dt_ordering_sorting.css') }}">


     <link href="{{ asset('plugins/charts/chartist/chartist.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/car-dashboard/style.css') }}" rel="stylesheet" type="text/css" />


    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    
</head>
<body class="default-sidebar">

<!-- start -->

<!-- End -->
        <div id="navHeadWrapper" class="navHeaderWrapper header-image">
        <div class="">
            <nav class="navbar navbar-expand-lg bg-faded header-nav">
                <div class="container">

                    <div class="col-xl-4 col-lg-3 col-6 mx-auto ">
                        <a class="navbar-brand" href="#"><h4>Go Label</h4></a>
                    </div>

                    <div class="col-6 text-right d-lg-none d-block">
                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#nav-content" aria-controls="nav-content" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon flaticon-left-menu"></span>
                        </button>
                    </div>

                    <div class="col-xl-8 col-lg-9">
                        <!-- Links -->
                        <div class="collapse navbar-collapse justify-content-end" id="nav-content">   
                            <ul class="navbar-nav text-center mt-lg-0 mt-5">
                                <li class="nav-item active">
                                    <a class="nav-link js-scroll-trigger" href="#navHeadWrapper">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link js-scroll-trigger" href="#whyusWrapper">Get Rate</a>
                                </li>                             
                              
                            </ul>
                            <form class="form-inline justify-content-lg-start justify-content-center mt-lg-0 mt-5">
                                <button class="btn ml-xl-4" type="submit">Try Now</button>
                            </form>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
      </div>
    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">
      <div id="content" class="main-content">
            <div class="container">
            


                <!-- CONTENT AREA -->
                

                     @yield('content')


                <!-- CONTENT AREA -->

            </div>
        </div>
        <!--  END CONTENT PART  -->
    </div>
    <!-- END MAIN CONTAINER -->

    <!--  BEGIN FOOTER  -->
    <footer class="footer-section theme-footer">

        <div class="footer-section-1  sidebar-theme">
            <p class="bottom-footer">Go label Leads</p>
        </div>

        <div class="footer-section-2 container-fluid">
            <div class="row">
                <div id="toggle-grid" class="col-xl-7 col-md-6 col-sm-6 col-12 text-sm-left text-center">
                
                </div>
               
            </div>
        </div>
    </footer>
    <!--  END FOOTER  -->

    <!--  BEGIN CONTROL SIDEBAR  -->


    <!--  END CONTROL SIDEBAR  -->

    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="{{ asset('assets/js/libs/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('plugins/blockui/jquery.blockUI.min.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->


   
     <!-- END GLOBAL MANDATORY SCRIPTS -->
    
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('plugins/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ asset('plugins/dropzone/custom-dropzone.js') }}"></script>
    <script src="{{ asset('plugins/file-upload/file-upload-with-preview.js') }}"></script>

    <!-- BEGIN PAGE LEVEL CUSTOM SCRIPTS -->
    <script src="{{ asset('plugins/charts/chartist/chartist.js') }}"></script>

    <script src="{{ asset('plugins/calendar/pignose/moment.latest.min.js') }}"></script>
    <script src="{{ asset('plugins/calendar/pignose/pignose.calendar.js') }}"></script>
    <script src="{{ asset('plugins/progressbar/progressbar.min.js') }}"></script>
    <script src="{{ asset('assets/js/car-dashboard/car-custom.js') }}"></script>

    <script>
        //First upload
        var firstUpload = new FileUploadWithPreview('myFirstImage')
        //Second upload
        var secondUpload = new FileUploadWithPreview('mySecondImage')
    </script>
    <!-- END PAGE LEVEL PLUGINS -->    
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('plugins/table/datatable/datatables.js') }}"></script>
    <script>        
        $('#default-ordering').DataTable( {
            "language": {
                "paginate": { "previous": "<i class='flaticon-arrow-left-1'></i>", "next": "<i class='flaticon-arrow-right'></i>" },
                "info": "Showing page _PAGE_ of _PAGES_"
            }, "order": [[ 3, "desc" ]],
            drawCallback: function () { $('.dataTables_paginate > .pagination').addClass(' pagination-style-13 pagination-bordered mb-5'); }
        } );
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
</body>
</html>