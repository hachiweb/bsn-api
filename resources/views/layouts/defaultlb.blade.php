<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
	<title>Go Label </title>
	<link rel="icon" type="image/x-icon" href="{{ asset('assets/img/favicon.ico') }}"/>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
	<link href="{{ asset('plugins/sliders/owlCarousel/css/owl.carousel.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/sliders/owlCarousel/css/owl.theme.default.min.css') }}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/flaticon/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/landing-page/style.css') }}">
</head>
<body>

	<div id="navHeadWrapper" class="navHeaderWrapper header-image">
		<!-- NavBar -->
		<!-- Brand -->
		<div class="">
			<nav class="navbar navbar-expand-lg bg-faded header-nav">
				<div class="container">

					<div class="col-xl-4 col-lg-3 col-6 mx-auto ">
						<a class="navbar-brand" href="#"><h4>Go Label</h4></a>
					</div>

					<div class="col-6 text-right d-lg-none d-block">
						<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#nav-content" aria-controls="nav-content" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon flaticon-left-menu"></span>
						</button>
					</div>

					<div class="col-xl-8 col-lg-9">
						<!-- Links -->
						<div class="collapse navbar-collapse justify-content-end" id="nav-content">   
							<ul class="navbar-nav text-center mt-lg-0 mt-5">
								<li class="nav-item active">
						        	<a class="nav-link js-scroll-trigger" href="#navHeadWrapper">Home</a>
						      	</li>
						      	<li class="nav-item">
						        	<a class="nav-link js-scroll-trigger" href="#whyusWrapper">Get Rate</a>
						      	</li>						      
						      
							</ul>
							<form class="form-inline justify-content-lg-start justify-content-center mt-lg-0 mt-5">
						      	<button class="btn ml-xl-4" type="submit">Try Now</button>
						    </form>
						</div>
					</div>
				</div>
			</nav>
		</div>
		<!-- /NavBar -->

		<!-- Header -->
		<div id="headerWrapper" class="container">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mt-lg-0 mt-5">
				<div class="row">
					<div class="col-lg-6 col-md-12 col-sm-12 col-12 order-lg-1 order-2 align-self-center  mb-lg-0 mb-5">
						<div class="site-header-inner  mt-lg-0 mt-5">
							<h2 class="">Go Label</h2>
							<p>Get Rate and print label</p>
							
						</div>
					</div>
					<div class="col-lg-6 col-md-12 col-sm-12 col-12 order-lg-2 order-1 text-center">
						<div class="banner-image">
							<img alt="cover-image" src="/golabel/public/assets/img/cover.png" class="img-fluid cover" style="">
							<img alt="cover-image" src="/golabel/public/assets/img/code.png" class="img-fluid code" style="">
							<img alt="cover-image" src="/golabel/public/assets/img/cloud.png" class="img-fluid cloud" style="">
							<img alt="cover-image" src="/golabel/public/assets/img/globe.png" class="img-fluid globe" style="">

							<img alt="cover-image" src="/golabel/public/assets/img/wave-1.png" class="img-fluid wave-1" style="">
							<img alt="cover-image" src="/golabel/public/assets/img/wave-2.png" class="img-fluid wave-2" style="">
							<img alt="cover-image" src="/golabel/public/assets/img/wave-3.png" class="img-fluid wave-3" style="">
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Header -->
	</div>

	<!-- Why Choose Us -->
	
	<!-- Welcome to our agency -->

	<!-- Chkout Features -->
	
	<!-- Chkout Features -->

 	<!-- Our Thoughts -->
	
	<!-- /Our Thoughts -->

	<!-- Services -->

	<!-- /Services -->

	<!-- Pricing -->
	
	<!-- /Pricing -->	

	<!-- Testimonials -->
	
	<!-- /Testimonials -->

	<!-- Latest News -->
	
	<!-- /Latest News -->

	<!-- Partners -->
	
	<!-- /Partners -->


	<!-- Subscribe us -->
	
	<!-- /Subscribe us -->
	    @yield('content')	
		<!-- Footer -->

	<!-- /Footer -->
   
	<!-- Mini Footer -->
	<div id="miniFooterWrapper" class="">
		<div class="container">
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12">
					<div class="position-relative">
						<div class="arrow text-center">
							<img alt="image-icon" src="/golabel/public/assets/img/footer-arrow.svg" class="img-fluid">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-5 mx-auto col-lg-6 col-md-6 site-content-inner text-md-left text-center copyright align-self-center">
							<p class="mt-md-0 mt-4 mb-0">© 2019 Go Label </p>
						</div>
						<div class="col-xl-5 mx-auto col-lg-6 col-md-6 site-content-inner text-md-right text-center align-self-center">
							<p class="mb-0">Tampa, FL </p>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>
	<!-- /Mini Footer -->

	<script src="{{ asset('assets/js/libs/jquery-3.1.1.min.js') }}"></script>
	<script src="{{ asset('bootstrap/js/popper.min.js') }}"></script>
	<script src="{{ asset('plugins/sliders/owlCarousel/js/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/js/pages/landing-page/script.js') }}"></script>
</body>
</html>