
@extends('layouts.default')

@section('content')



                <div class="row">

                    <div class="col-lg-12">
                        <div class="statbox widget box box-shadow">
                            <div class="widget-header widget-heading">
                                <div class="row">
                                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                        <h4>Top Drivers</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
						<div class="col-xl-4 col-lg-4 col-md-4 col-12 layout-spacing">
	                        <div class="widget-content widget-content-area h-100 br-4">

	                            <div class="widget-card top-drivers">
	                                <div class="row">
	                                    <div class="col-md-12 mb-5">
	                                        <h5>Is address valid</h5>
	                                    </div>
	                                    <div class="col-md-12 text-center">

	                                        <div class="usr-meta-img mb-4 mx-auto">
	                                  
	                                        </div>

	                                        <div class="t-d-drive-stats mb-4">
	                                            <p class="mb-0">

																@if($result->validation_results->is_valid) 
	                                            						Yes
				                                            	@else
																    	No
																@endif
	                                            </p>
	                                            
	                                        </div>

	                                    </div>
	                             
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    	<div class="col-xl-4 col-lg-4 col-md-4 col-12 layout-spacing">
	                        <div class="widget-content widget-content-area h-100 br-4">

	                            <div class="widget-card top-drivers">
	                                <div class="row">
	                                    <div class="col-md-12 mb-5">
	                                        <h5>Is request Complete</h5>
	                                    </div>
	                                    <div class="col-md-12 text-center">

	                                        <div class="usr-meta-img mb-4 mx-auto">
	                                  
	                                        </div>

	                                        <div class="t-d-drive-stats mb-4">
	                                            <p class="mb-0">
																@if($result->is_complete) 
	                                            						Yes
				                                            	@else
																    	No
																@endif
	                                            </p>
	                                            
	                                        </div>

	                                    </div>
	                             
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    	<div class="col-xl-4 col-lg-4 col-md-4 col-12 layout-spacing">
	                        <div class="widget-content widget-content-area h-100 br-4">

	                            <div class="widget-card top-drivers">
	                                <div class="row">
	                                    <div class="col-md-12 mb-5">
	                                        <h5>Is address residential</h5>
	                                    </div>
	                                    <div class="col-md-12 text-center">

	                                        <div class="usr-meta-img mb-4 mx-auto">
	                                  
	                                        </div>

	                                        <div class="t-d-drive-stats mb-4">
	                                            <p class="mb-0">
	                                            				@if($result->is_residential) 
	                                            						Yes
				                                            	@else
																    	No
																@endif
	                                            </p>
	                                            
	                                        </div>

	                                    </div>
	                             
	                                </div>
	                            </div>
	                        </div>
	                    </div>

					
					 </div>



@endsection