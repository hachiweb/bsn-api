
@extends('layouts.default')

@section('content')

@if ($transaction['status'] == 'SUCCESS')
   Shipping label url:     <a href="{{url($transaction['label_url'])}}" >{{$transaction['label_url']}}</a> <br>

   Shipping tracking number: {{ $transaction['tracking_number']}}
@else
    Transaction failed with messages:
    foreach ($transaction['messages'] as $message) {
        {{ $message }}
    }
@endif




@endsection