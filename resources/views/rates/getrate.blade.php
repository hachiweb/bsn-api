
@extends('layouts.default')

@section('content')
 <div class="container">
                <div class="page-header">
                    <div class="page-title">
                        <h3>Form Layouts <small>Default</small></h3>
                        <div class="crumbs">
                            <ul id="breadcrumbs" class="breadcrumb">
                                <li><a href="index.html"><i class="flaticon-home-fill"></i></a></li>
                                <li><a href="#">Get</a></li>
                                <li><a href="#">Rate</a> </li>                              
                            </ul>
                        </div>
                    </div>
                </div>

<div class="row">
                    <div class="col-lg-6 layout-spacing">
                        <div class="statbox widget box box-shadow">
                            <div class="widget-header">                                
                                <div class="row">
                                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                        <h4>From Address</h4>
                                    </div>                                                                        
                                </div>
                            </div>
                            <div class="widget-content widget-content-area">
                           <form action="{{ url('Rates/getrateprocess') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">  
                            @csrf
                                         <div class="row mb-4">
                                                <div class="col-sm-12 col-12">
                                                    <input type="text" name="frname" class="form-control mb-4" placeholder="frname*">
                                                </div>
                                            </div>
                                              <div class="row mb-4">
                                                <div class="col-sm-12 col-12">
                                                    <input type="text" name="frcompany" class="form-control mb-4" placeholder="frcompany*">
                                                </div>
                                            </div>
                                              <div class="row mb-4">
                                                <div class="col-sm-12 col-12">
                                                    <input type="text" name="frstreet1" class="form-control mb-4" placeholder="frstreet1*">
                                                </div>
                                             </div>
                                             <div class="row mb-4">
                                                <div class="col-sm-12 col-12">
                                                    <input type="text" name="frphone" class="form-control mb-4" placeholder="frphone*">
                                                </div>
                                            </div>
                                              <div class="row mb-4">
                                                <div class="col-sm-12 col-12">
                                                    <input type="text" name="fremail" class="form-control mb-4" placeholder="fremail*">
                                                </div>
                                            </div>
                                            <div class="row mb-4">
                                                <div class="col-sm-6 col-12 mb-4 mb-sm-0">
                                                    <input type="text" name="frcity" class="form-control mb-4" placeholder="frcity*">
                                                </div>
                                                <div class="col-sm-6 col-12">
                                                    <input type="text" name="frstate" class="form-control mb-4" placeholder="frstate*">
                                                </div>
                                            </div>
                                             <div class="row mb-4">
                                                <div class="col-sm-6 col-12 mb-4 mb-sm-0">
                                                    <input type="text" name="frpostcode" class="form-control mb-4" placeholder="frpostcode*">
                                                </div>
                                                <div class="col-sm-6 col-12">
                                                    <input type="text" name="frcountry" class="form-control mb-4" placeholder="frcountry*">
                                                </div>
                                            </div>
                                 
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-6 layout-spacing">
                        <div class="statbox widget box box-shadow">
                            <div class="widget-header">                                
                                <div class="row">
                                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                        <h4>To Address</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-content widget-content-area">
                                <div class="row mb-3">
                                    <div class="col-sm-12 col-12">
                                        <input type="text" name="name" class="form-control mb-4" placeholder="name*">
                                    </div>
                                </div>
                                  <div class="row mb-4">
                                    <div class="col-sm-12 col-12">
                                        <input type="text" name="company" class="form-control mb-4" placeholder="company*">
                                    </div>
                                </div>
                                  <div class="row mb-4">
                                    <div class="col-sm-12 col-12">
                                        <input type="text" name="street1" class="form-control mb-4" placeholder="street1*">
                                    </div>
                                </div>
                                  <div class="row mb-4">
                                    <div class="col-sm-12 col-12">
                                        <input type="text" name="street1" class="form-control mb-4" placeholder="street1*">
                                    </div>
                                </div>
                                  <div class="row mb-4">
                                    <div class="col-sm-12 col-12">
                                        <input type="text" name="email" class="form-control mb-4" placeholder="email*">
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col-sm-6 col-12 mb-4 mb-sm-0">
                                        <input type="text" name="city" class="form-control mb-4" placeholder="city*">
                                    </div>
                                    <div class="col-sm-6 col-12">
                                        <input type="text" name="state" class="form-control mb-4" placeholder="state*">
                                    </div>
                                </div>
                                 <div class="row mb-4">
                                    <div class="col-sm-6 col-12 mb-4 mb-sm-0">
                                        <input type="text" name="postcode" class="form-control mb-4" placeholder="postcode*">
                                    </div>
                                    <div class="col-sm-6 col-12">
                                        <input type="text" name="country" class="form-control mb-4" placeholder="country*">
                                    </div>
                                </div>
                              
                            </div>
                        </div>
                    </div>
                </div>

               <div class="row">
                    <div class="col-lg-12 layout-spacing">
                        <div class="statbox widget box box-shadow">
                            <div class="widget-header">
                                <div class="row">
                                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                        <h4>Package dimension</h4>
                                    </div>                                                                        
                                </div>
                            </div>
                            <div class="widget-content widget-content-area">
                                                          
                                    <div class="form-row mb-4">
                                        <div class="form-group col-md-4">
                                            <label for="inputCity">length</label>
                                            <input type="text" name="length" class="form-control" id="length">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="inputCity">width</label>
                                            <input type="text" name="width" class="form-control" id="width">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="inputZip">height</label>
                                            <input type="text" name="height" class="form-control" id="height">
                                        </div>
                                  </div>
                                  <div class="form-row mb-4">
                                        <div class="form-group col-md-4">
                                            <label for="inputCity">distance_unit</label>
                                            <input type="text" name="distance_unit" class="form-control" id="distance_unit">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="inputCity">weight</label>
                                            <input type="text" name="weight" class="form-control" id="weight">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="inputZip">mass_unit</label>
                                            <input type="text" name="mass_unit" class="form-control" id="mass_unit">
                                        </div>
                                    </div>
                                    
                                  <button type="submit" class="btn btn-button-7 mb-4 mt-3">Sign in</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
 </div>
@endsection