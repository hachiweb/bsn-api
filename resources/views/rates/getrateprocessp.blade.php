
@extends('layouts.default')

@section('content')

 <div class="row">

   			<div class="col-lg-12 col-md-12 layout-spacing">
                        <div class="statbox widget box box-shadow">
                            <div class="widget-header">
                                <div class="row">
                                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                        <h4>Available rates:</h4>
                                    </div>                 
                                </div>
                            </div>
                            <div class="widget-content widget-content-area">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped mb-4">
                                        <thead>
                                            <tr>
                                                <th>provider</th>
                                                <th>provider name</th>
                                                <th>Amount</th>
                                                <th>Days to delivery</th>
                                                <th>estimated days</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           @php $index=1 @endphp

                                             @foreach($rates as $rate)
                                            
                                            <tr>
                                                <td>{{$rate['provider']}}</td>
                                                <td>{{$rate['servicelevel']['name']}}</td>
                                                <td>{{ $rate['amount']}} </td>
                                                <td>{{$rate['duration_terms']}} </td>
                                                <td>{{$rate['estimated_days']}} </td>
                                                <td><a href="{{action('RatesController@getlabel', $index)}}" class="btn btn-warning">get Label</a></td>
                                            </tr>
                                              
                                               @php $index++ @endphp
                                            @endforeach



                                      </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

     </div>

@endsection