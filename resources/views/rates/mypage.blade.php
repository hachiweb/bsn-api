

@extends('layouts.defaultlb')

@section('content')

	 <form action="{{ url('Rates/checkaddress') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">  
  		@csrf
					<div id="footerWrapper" class="">
						<div class="container-fluid">
							<div class="row">
								<div class="col-xl-7 mx-auto col-lg-12 col-md-12">
									<div class="row">
										<div class="col-xl-6 col-lg-6 col-md-6 site-content-inner text-md-left text-center copyright">
											<p class="mb-5"><span>Address</span></p>
											<h2 class="">Check your Address <br/> with us</h2>
										</div>
										<div class="col-xl-6 col-lg-6 col-md-6 site-content-inner text-md-right text-center">
											<form class="mb-4" action="javascript:void(0);">
					                           
					                            <div class="row mb-4">
					                                <div class="col-sm-12 col-12">
					                                    <input type="text" name="name" class="form-control mb-4" placeholder="name*">
					                                </div>
					                            </div>
					                              <div class="row mb-4">
					                                <div class="col-sm-12 col-12">
					                                    <input type="text" name="company" class="form-control mb-4" placeholder="company*">
					                                </div>
					                            </div>
					                              <div class="row mb-4">
					                                <div class="col-sm-12 col-12">
					                                    <input type="text" name="street1" class="form-control mb-4" placeholder="street1*">
					                                </div>
					                            </div>
					                              <div class="row mb-4">
					                                <div class="col-sm-12 col-12">
					                                    <input type="text" name="email" class="form-control mb-4" placeholder="email*">
					                                </div>
					                            </div>
					                            <div class="row mb-4">
					                                <div class="col-sm-6 col-12 mb-4 mb-sm-0">
					                                    <input type="text" name="city" class="form-control mb-4" placeholder="city*">
					                                </div>
					                                <div class="col-sm-6 col-12">
					                                    <input type="text" name="state" class="form-control mb-4" placeholder="state*">
					                                </div>
					                            </div>
					                             <div class="row mb-4">
					                                <div class="col-sm-6 col-12 mb-4 mb-sm-0">
					                                    <input type="text" name="postcode" class="form-control mb-4" placeholder="postcode*">
					                                </div>
					                                <div class="col-sm-6 col-12">
					                                    <input type="text" name="country" class="form-control mb-4" placeholder="country*">
					                                </div>
					                            </div>



					                            <div class="row">
					                                <div class="col text-sm-left text-center">
					                                    <button class="btn btn-primary mt-4">Send Message</button>
					                                </div>
					                            </div>
					                        </form>
										</div>
									</div>
								</div>		
							</div>
						</div>
					</div>
	      {{ Form::close() }}
@endsection