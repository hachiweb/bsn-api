<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


    Route::get('/Rates', 'RatesController@index');

    Route::get('/Rates/getrate', 'RatesController@getRate');
    Route::get('/Rates/mypage', 'RatesController@mypage');
    Route::post('/Rates/checkaddress', 'RatesController@checkaddress');
    Route::get('/Rates/checkaddressrep', 'RatesController@checkaddressrep');
    Route::post('/Rates/getrateprocess', 'RatesController@getrateprocess');
    Route::get('/Rates/getlabel/{index}', 'RatesController@getlabel');
    Route::post('/Rates/processlabel', 'RatesController@processlabel');